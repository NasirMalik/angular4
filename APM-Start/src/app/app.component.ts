import { Component } from '@angular/core';
import {ProductService} from './products/product.service';
import {JobService} from './jobs/job.service';

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ProductService, JobService]
})
export class AppComponent {
  title = 'Product Management';
}
