import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import {ProductListComponent} from './products/product-list.component';
import {CodeFormatterPipe} from '../transformer/codeFormater';
import {StarRatingComponent} from './starRating/star-rating.component';
import { HttpModule } from '@angular/http';
import { ProductDetailComponent } from './products/product-detail.component';
import {WelcomeComponent} from './home/welcome.component';
import {RouterModule} from '@angular/router';
import {JobListComponent} from './jobs/job-list.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    CodeFormatterPipe,
    StarRatingComponent,
    ProductDetailComponent,
    WelcomeComponent,
    JobListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: 'products', component: ProductListComponent },
      { path: 'products/:id', component: ProductDetailComponent },
      { path: 'jobs', component: JobListComponent },
      { path: 'welcome', component: WelcomeComponent },
      { path: '', redirectTo: 'welcome', pathMatch: 'full' },
      { path: '**', redirectTo: 'welcome', pathMatch: 'full' }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
