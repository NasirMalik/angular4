import {Component, OnInit} from '@angular/core';
import {IJob} from './job';
import {JobService} from './job.service';
/**
 * Created by nasir on 27/9/17.
 */

@Component ({
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})

export class JobListComponent implements OnInit {
  pageTitle: string = 'Job List';
  jobs: IJob[] = [];

  constructor(private _jobService: JobService) {

  }

  ngOnInit(): void {
    // this.products = this._productService.getProducts();
    this._jobService.getJobs()
      .subscribe(ijobs => {
        this.jobs = ijobs;
      });
  }

}
