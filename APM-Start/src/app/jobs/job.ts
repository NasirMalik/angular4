/**
 * Created by nasir on 27/9/17.
 */
export interface IJob {
  id: number;
  jobName: number;
  jobType: string;
  jobDesc: string;
}
