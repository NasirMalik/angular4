import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import {IJob} from './job';
/**
 * Created by nasir on 27/9/17.
 */

@Injectable()
export class JobService {

  constructor(private _http: Http) { }

  getJobs(): Observable<IJob[]> {
    return this._http.get('http://localhost:8090/schedule/jobs')
      .map((response: Response) => <IJob[]> response.json());
    // .do(data => console.log(JSON.stringify(data)));
  }

}
