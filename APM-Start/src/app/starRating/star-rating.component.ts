/**
 * Created by nasir on 4/9/17.
 */
import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';

@Component ({
  selector: 'pm-star',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnChanges {

  @Input() rating: number;
  componentWidth: number;
  @Output() clicked: EventEmitter<string> = new EventEmitter<string>();

  ngOnChanges(): void {
    this.componentWidth = this.rating * 86 / 5;
  }
  onClick(): void {
    this.clicked.emit('Clicked : Rating = ' + this.rating);
  }

}
