/**
 * Created by nasir on 5/9/17.
 */

import {Injectable} from '@angular/core';
import {IProduct} from './product';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';


@Injectable ()
export class ProductService {

  constructor(private _http: Http) { }

  getProducts(): Observable<IProduct[]> {
    return this._http.get('http://localhost:8090/data/products')
      .map((response: Response) => <IProduct[]> response.json());
      // .do(data => console.log(JSON.stringify(data)));
  }

  getProduct(productId: string): Observable<IProduct> {
    return this._http.get('http://localhost:8090/data/product/' + productId)
      .map((response: Response) => <IProduct> response.json());
  }


}
