import {Component, Input, OnInit} from '@angular/core';
import {IProduct} from './product';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from './product.service';

@Component({
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  productId: string;
  pageTitle: string = 'Product Detail';
  product: IProduct;

  constructor(private _route: ActivatedRoute,
              private _service: ProductService,
              private _router: Router) {
    this.productId = this._route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this._service.getProduct(this.productId)
      .subscribe(iproduct => {
        this.product = iproduct;
      });
  }

  onBack(): void {
    this._router.navigate(['/products']);
  }

}
