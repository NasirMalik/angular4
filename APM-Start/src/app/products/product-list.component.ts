/**
 * Created by nasir on 28/8/17.
 */
import {Component, OnInit} from '@angular/core';
import {IProduct} from './product';
import {ProductService} from './product.service';

@Component ({
  // selector: 'pm-products',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {
  pageTitle: string = 'Product List';
  imageWidth: number = 50;
  imageMargin: number = 2;
  showImage: boolean = false;

  _productFilter: string = '';
  get productFilter(): string {
    return this._productFilter;
  }
  set productFilter(value: string){
    this._productFilter = value;
    this.filterProducts();
  }


  filteredProducts: IProduct[];
  products: IProduct[] = [];

  constructor(private _productService: ProductService) {

  }

  toggleImages(): void {
    this.showImage = !this.showImage;
  }

  ngOnInit(): void {
    // this.products = this._productService.getProducts();
    this._productService.getProducts()
      .subscribe(iproducts => {
        this.products = iproducts;
        this.filteredProducts = this.products;
      });
  }

  filterProducts(): void {
    console.log('In filterProducts');
    this.filteredProducts = this.products.filter((product: IProduct) =>
          product.productName.indexOf(this.productFilter) !== -1);
  }

  onRatingClicked(message: string): void {
    this.pageTitle = 'Product List ' + message;
  }

}
