/**
 * Created by Nasir on 1/09/2017.
 */
import {Pipe, PipeTransform} from "@angular/core";

@Pipe ({
  name: `codeFormatter`
})
export class CodeFormatterPipe implements PipeTransform {
  transform(value: string,
            ch: string): string{
    return ch + value;
  }
}
