import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {JobService} from '../../api/service/job.service';
import {IJobQ} from '../../api/model/jobQ';

@Component({
  templateUrl: 'schedule-list.component.html',
  styleUrls: ['schedule-list.component.css']
})
export class ScheduleListComponent implements OnInit {

  jobId: string;
  jobQ: IJobQ[];

  constructor(private _route: ActivatedRoute,
              private _service: JobService,
              private _router: Router) {
    this.jobId = this._route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this._service.getJobsQuedByJobId(this.jobId)
      .subscribe(ijobQ => {
        this.jobQ = ijobQ;
      });
  }

}
