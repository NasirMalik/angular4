import {Component, OnInit} from '@angular/core';
import {IJobQStatus} from '../../api/model/jobQStatus';
import {JobService} from '../../api/service/job.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/take';
import {PieChartConfig} from '../../api/config/PieChartConfig';
import {PieChartComponent} from './PieChartComponent';

@Component({
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  jobQStatus: IJobQStatus[];
  showHideList: boolean = false
  showHideFilter: boolean = false;
  counter: number = 300;
  countDown: any;

  data1: any[];
  config1: PieChartConfig;
  elementId1: string;

  constructor(private _service: JobService, private _pieChartComp: PieChartComponent) {

  }

  ngOnInit() {
    this.config1 = new PieChartConfig('', 0.4);
    this.elementId1 = 'myPieChart1';
    this.refreshData();
    setInterval(() => {
      this.refreshData();
    }, 300000);
  }

  refreshData() {
    this._service.getStatus()
      .subscribe(statusMap => {
        this.jobQStatus = statusMap.statusList;
        this.data1 = statusMap.statusArray;
        this._pieChartComp.drawChart(this.elementId1, this.data1, this.config1);
      });
    this.counter = 300;
    this.countDown = Observable.timer(0, 10000)
      .take(this.counter)
      .map(() => this.counter -= 10);
  }
}
