/**
 * Created by nasir on 27/11/17.
 */

import { Component, Input, OnInit } from '@angular/core';
import { PieChartConfig } from '../../api/config/PieChartConfig';
import { PieChartService } from '../../api/service/PieChartService';


declare var google: any;


@Component({
  selector: 'pie-chart',
  templateUrl: './piechart.component.html'
})
export class PieChartComponent {

  constructor(private _pieChartService: PieChartService) {}

  drawChart(elementId: string, data: any[], config: PieChartConfig): void {
    this._pieChartService.buildPieChart(elementId, data, config);
  }
}
