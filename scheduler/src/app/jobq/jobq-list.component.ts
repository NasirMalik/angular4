import {Component, OnInit} from '@angular/core';
import {IJobQ} from '../../api/model/jobQ';
import {JobService} from '../../api/service/job.service';
/**
 * Created by nasir on 27/9/17.
 */

@Component ({
  templateUrl: './jobq-list.component.html',
  styleUrls: ['./jobq-list.component.css']
})

export class JobQListComponent implements OnInit {
  pageTitle: string = 'Job Q';
  jobQ: IJobQ[] = [];

  constructor(private _jobService: JobService) {

  }

  ngOnInit(): void {
    this._jobService.getJobQ()
      .subscribe(ijobQ => {
        this.jobQ = ijobQ;
      });
  }

}
