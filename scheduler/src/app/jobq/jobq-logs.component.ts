import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {JobService} from '../../api/service/job.service';
import {IJobQ} from '../../api/model/jobQ';
import {IJobLog} from '../../api/model/jobLog';

@Component({
  templateUrl: './jobq-logs.component.html',
  styleUrls: ['./jobq-logs.component.css']
})
export class JobQDetailComponent implements OnInit {

  jobId: string;
  page: string;
  pageTitle: string = 'Job Q';
  jobQued: IJobQ;
  jobLogs: IJobLog[];
  jobLogMap: IJobLog[][];

  constructor(private _route: ActivatedRoute,
              private _service: JobService,
              private _router: Router) {
    this.jobId = this._route.snapshot.paramMap.get('id');
    this.page = this._route.snapshot.paramMap.get('page');
  }

  ngOnInit() {
    this._service.getJobQued(this.jobId)
      .subscribe(ijobQ => {
        this.jobQued = ijobQ;
      });

    this._service.getLogs(this.jobId)
      .subscribe(ijobLog => {
        this.jobLogMap = ijobLog;
      });
  }

  onBack(): void {
    this._router.navigate(['/' + this.page]);
  }

}
