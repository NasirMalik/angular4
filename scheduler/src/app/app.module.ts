import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import {WelcomeComponent} from './home/welcome.component';
import {RouterModule} from '@angular/router';
import {JobListComponent} from './jobs/job-list.component';
import {JobDetailComponent} from './jobs/job-detail.component';
import {JobQListComponent} from './jobq/jobq-list.component';
import {JobQDetailComponent} from './jobq/jobq-logs.component';
import {ScheduleListComponent} from './scheduler/schedule-list.component';
import {PieChartComponent} from './home/PieChartComponent';


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    JobListComponent,
    JobDetailComponent,
    JobQListComponent,
    JobQDetailComponent,
    ScheduleListComponent,
    PieChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: 'jobs', component: JobListComponent },
      { path: 'jobs/:id', component: JobDetailComponent },
      { path: 'jobQ', component: JobQListComponent },
      { path: 'jobQ/:id/:page', component: JobQDetailComponent},
      { path: 'viewSchedule/:id', component: ScheduleListComponent},
      { path: 'welcome', component: WelcomeComponent },
      { path: '', redirectTo: 'welcome', pathMatch: 'full' },
      { path: '**', redirectTo: 'welcome', pathMatch: 'full' }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
