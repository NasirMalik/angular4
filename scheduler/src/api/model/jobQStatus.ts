import {IJobQ} from './jobQ';
/**
 * Created by nasir on 21/11/17.
 */

export interface IJobQStatus {
  id: number;
  scheduledFor: Date;
  lastUpdated: Date;
  status: string;
  statusDetails: string;
  jobQ: IJobQ;
}
