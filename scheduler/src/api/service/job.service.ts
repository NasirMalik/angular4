import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import {IJob} from '../model/job';
import {IJobQ} from '../model/jobQ';
import {IJobLog} from '../model/jobLog';
import {IJobQStatus} from "../model/jobQStatus";
/**
 * Created by nasir on 27/9/17.
 */

@Injectable()
export class JobService {

  serverUrl: string = 'http://localhost:8082';

  constructor(private _http: Http) {  }

  getJobs(): Observable<IJob[]> {
    return this._http.get(this.serverUrl + '/schedule/jobs')
      .map((response: Response) => <IJob[]> response.json());
  }

  getJob(jobId: string): Observable<IJob> {
    return this._http.get(this.serverUrl + '/schedule/jobs/' + jobId)
      .map((response: Response) => <IJob> response.json());
  }

  getJobQ(): Observable<IJobQ[]> {
    return this._http.get(this.serverUrl + '/schedule/jobQ')
      .map((response: Response) => <IJobQ[]> response.json());
  }

  getJobQued(jobQId: string): Observable<IJobQ> {
    return this._http.get(this.serverUrl + '/schedule/jobQ/' + jobQId)
      .map((response: Response) => <IJobQ> response.json());
  }

  getJobsQuedByJobId(jobId: string): Observable<IJobQ[]> {
    return this._http.get(this.serverUrl + '/schedule/jobQ/job/' + jobId)
      .map((response: Response) => <IJobQ[]> response.json());
  }

  getLogs(jobQId: string): Observable<IJobLog[][]> {
    return this._http.get(this.serverUrl + '/schedule/jobQ/log/' + jobQId)
      .map((response: Response) => <IJobLog[][]> response.json());
  }

  // getStatus(): Observable<IJobQStatus[]> {
  //   return this._http.get(this.serverUrl + '/schedule/jobQ/status')
  //     .map((response: Response) => <IJobQStatus[]> response.json());
  //   // .do(data => console.log(JSON.stringify(data)));
  // }

  getStatus(): Observable<any> {
    return this._http.get(this.serverUrl + '/schedule/jobQ/status')
      .map((response: Response) => <any> response.json());
    // .do(data => console.log(JSON.stringify(data)));
  }

}
