/**
 * Created by nasir on 27/11/17.
 */

declare var google: any;

export class BaseChartsService {
  constructor() {
    google.charts.load('current', {'packages': ['corechart']});
  }

  protected buildChart(data: any[], chartFunc: any, options: any): void {
    let func = (chartFunc, options) => {
      let datatable = google.visualization.arrayToDataTable(data);
      chartFunc().draw(datatable, options);
    };
    let callback = () => func(chartFunc, options);
    google.charts.setOnLoadCallback(callback);
  }

}
