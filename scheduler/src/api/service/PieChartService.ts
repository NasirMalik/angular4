/**
 * Created by nasir on 27/11/17.
 */

import { Injectable } from '@angular/core';
import { PieChartConfig } from '../config/PieChartConfig';
import { BaseChartsService } from './BaseChartsService';

declare var google: any;

@Injectable()
export class PieChartService extends BaseChartsService {

  constructor() { super(); }

  public buildPieChart(elementId: string, data: any[], config: PieChartConfig) : void {
    var chartFunc = () => { return new google.visualization.PieChart(document.getElementById(elementId)); };
    var options = {
      title: config.title,
      pieHole: 0.2,
      legend: config.legend,
      pieSliceText: 'none',
      slices: {1: {color: '#abebc6'}, 0: {color: '#e59866'}}
    };

    this.buildChart(data, chartFunc, options);
  }
}
